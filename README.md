Setup:

Clone repo:

    cd 
    git clone git@gitlab.com:hexfaker/dotfiles.git .dotfiles


Run init

    cd .dotfiles
    ./init.sh
