shopt -s expand_aliases

aalias uctl "systemctl --user"
aalias sctl "sudo systemctl"

aalias cp "cp -i"                          # confirm before overwriting something

aalias df 'df -h'                          # human-readable sizes
aalias du 'dust'                          # human-readable sizes

aalias free 'free -m'                      # show sizes in MB
aalias more less

aalias doco "docker-compose"
aalias ta "tmux attach -t common"

aalias rsyncp "rsync --info=progress2 -rha" 
aalias vim nvim
