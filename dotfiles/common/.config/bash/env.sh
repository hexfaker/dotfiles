if [[ -d /home/linuxbrew/ ]]; then
   eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi

PATH=$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.local/xbin:$PATH

export EDITOR=vim
export PAGER="less -S"

export IPYTHONDIR=~/.config/ipython

#if command -v pyenv 1>/dev/null 2>&1; then
#  eval "$(pyenv init -)"
#fi

#if which pyenv-virtualenv-init > /dev/null; then 
#  eval "$(pyenv virtualenv-init -)"
#fi
