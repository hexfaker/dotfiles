#
# ~/.bashrc
#

#if [[ $- != *i* ]] ; then
#  # shell is non-interactive. be done now!
#  return
#fi

# Load all files from .config/bash directory
if [ -d $HOME/.config/bash ]; then
  for file in $HOME/.config/bash/*.sh; do
    source $file
  done
fi

# Load all custom completions
if [ -d $HOME/.config/bash/completions ]; then
  for file in $HOME/.config/bash/completions/*; do
    source $file
  done
fi

