#!/usr/bin/env python
from pathlib import Path
from typing import Optional
from imageio import mimwrite, imread
import typer
from typer import Typer

app = Typer()

@app.command()
def make_video(root: Path = typer.Argument(Path('.')), video_path: Optional[Path] = None, glob: str = typer.Option("*.jpg")):

    if video_path is None:
        video_path = root / "vid.mp4"

    frames = sorted([*root.glob(glob)])

    mimwrite(video_path, [imread(p) for p in frames])


app()
