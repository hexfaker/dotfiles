#!/usr/bin/env python3
import gi
gi.require_version('Playerctl', '2.0')

from gi.repository import Playerctl, GLib
import os


manager = Playerctl.PlayerManager()

IS_PLAYING = {}

def set_status(player, remove=False):
    key = player.props.player_instance
    if not remove:
        value = player.props.playback_status == Playerctl.PlaybackStatus.PLAYING
        IS_PLAYING[key] = value
    else:
        del IS_PLAYING[key]

    if any(IS_PLAYING.values()):
        state = "true"
    else:
        state = "false"

    os.system(f"gsettings --schemadir ~/.local/share/gnome-shell/extensions/caffeine@patapon.info/schemas/ set org.gnome.shell.extensions.caffeine toggle-state {state}")   


def on_play(player, status, manager):
    set_status(player)

def init_player(name):
    # choose if you want to manage the player based on the name
    player = Playerctl.Player.new_from_name(name)
    player.connect('playback-status', on_play, manager)
    manager.manage_player(player)
    set_status(player)


def on_name_appeared(manager, name):
    init_player(name)


def on_player_vanished(manager, player):
    set_status(player, remove=True)


manager.connect('name-appeared', on_name_appeared)
manager.connect('player-vanished', on_player_vanished)

for name in manager.props.player_names:
    init_player(name)

main = GLib.MainLoop()
main.run()
